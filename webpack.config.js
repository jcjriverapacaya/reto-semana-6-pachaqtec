const path = require("path"),
    HtmlWebpackPlugin = require("html-webpack-plugin"),
    CopyPlugin = require("copy-webpack-plugin"),
    MiniCssExtractPlugin = require("mini-css-extract-plugin"),
    autoprefixer = require("autoprefixer"),
    { CleanWebpackPlugin } = require("clean-webpack-plugin");

module.exports = {
    entry: ["@babel/polyfill", "./src/app.js"],
    output: {
        filename: "bundle.[hash].js",
        path: path.join(__dirname, "/dist"),
    },
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        compress: true,
        port: 9000,
        open: true,
    },
    module: {
        rules: [
            {
                test: /\.hbs$/,
                use: "handlebars-loader",
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ["babel-loader", "eslint-loader"],
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    {
                        loader: "postcss-loader",
                        options: {
                            autoprefixer: {
                                browser: ["last 2 versions"],
                            },
                            plugins: () => [autoprefixer],
                        },
                    },
                    "sass-loader",
                ],
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: ["file-loader", { loader: "image-webpack-loader" }],
            },
            {
                test: /\.(ttf|eot|woff2?|mp3|mp4|txt|pdf|xml)$/i,
                use: "file-loader?name=data/[name].[ext]",
            },
        ],
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: "commons",
                    chunks: "all",
                },
            },
        },
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            filename: "index.html",
            template: "src/pages/index.hbs",
            minify: {
                html5: true,
                collapseWhitespace: false,
                caseSensitive: true,
                removeComments: true,
                removeEmptyElements: false,
            },
        }),
        new MiniCssExtractPlugin({
            filename: "index.[hash].css",
        }),
        new CopyPlugin([
            {
                from: "./src/assets/",
                to: "assets/",
                ignore: ["*.DS_Store"],
            },
        ]),
    ],
};
