export class Helpers {
    // Crear elementos con atributos e hijo
    createCustomElement(element, attributes, children) {
        let customElement = document.createElement(element);
        if (children !== undefined)
            children.forEach((el) => {
                if (el.nodeType) {
                    if (el.nodeType === 1 || el.nodeType === 11)
                        customElement.appendChild(el);
                } else {
                    customElement.innerHTML += el;
                }
            });
        this.addAttributes(customElement, attributes);
        return customElement;
    }

    // Añadir un objeto de atributos a un elemento
    addAttributes(element, attrObj) {
        for (let attr in attrObj) {
            if (attrObj.hasOwnProperty(attr))
                element.setAttribute(attr, attrObj[attr]);
        }
    }

    // Crear e imprimir modal
    createModal(content) {
        const modalContentEl = this.createCustomElement(
                "div",
                {
                    id: "modal-content",
                    class: "modal-content",
                },
                [content]
            ),
            modalEl = this.createCustomElement(
                "div",
                {
                    id: "modal-container",
                    class: "modal-container",
                },
                [modalContentEl]
            );

        // Imprimir modal
        document.body.appendChild(modalEl);

        // Remover modal
        const removeModal = () => document.body.removeChild(modalEl);

        // cerrar modal con click
        modalEl.addEventListener("click", (e) => {
            if (e.target === modalEl) removeModal();
        });

        // cerrar modal con escape
        const offCloseModalEsc = () =>
            removeEventListener("keyup", closeModalEsc);
        const closeModalEsc = (e) => {
            if (e.key === "Escape") {
                removeModal();
                offCloseModalEsc();
            }
        };
        addEventListener("keyup", closeModalEsc);
    }

    // obtener y recorrer los hermanos de un elemento para el Slider
    static indexForSiblings(el) {
        let children = el.parentNode.children;
        for (let i = 0; i < children.length; i++) {
            let child = children[i];
            if (child === el) return i;
        }
    }
}
